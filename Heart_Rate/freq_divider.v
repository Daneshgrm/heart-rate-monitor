module freq_divider(output signal,
					input clk
    );
	
	reg [11:0] cnt_tmp = 0;
	reg signal_reg = 0;
	assign signal = signal_reg;
	always@(posedge clk)
		begin
			if(cnt_tmp == 4096)	begin
				cnt_tmp = 0;
			end
			else	begin
				cnt_tmp = cnt_tmp + 1'b1;
			end
			if(cnt_tmp[10]) begin
				signal_reg = 1;
			end
			else	begin
				signal_reg = 0;
			end
		end
endmodule
