module binary_bcd(output [11:0] bcd_number,
				input clk,
				input [19:0] number
    );
	
	reg [11:0] bcd_number_tmp = 0;
	
	wire [1:0] ready;
	wire [19:0] quotient_1;
	wire [19:0]	remainder_1;
	wire [19:0] quotient_2;
	wire [19:0]	remainder_2;
	
	divider div_1(quotient_1, remainder_1, ready[0], clk, number, 100);
	divider div_2(quotient_2, remainder_2, ready[1], clk, remainder_1, 10);
	
	assign bcd_number = bcd_number_tmp;
	
	always@(posedge clk)
		begin
			if(ready[0] & ready[1])
				bcd_number_tmp <= {quotient_1[3:0] , quotient_2[3:0] ,remainder_2[3:0]};
		end
endmodule
