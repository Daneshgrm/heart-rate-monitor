`timescale 1ms / 1ps
module main_test;

	// Inputs
	reg clk;
	reg in_signal;
	reg mute;

	// Outputs
	wire [19:0] bpm;
	wire led;
	wire beep;

	// Instantiate the Unit Under Test (UUT)
	main_module uut (
		.clk(clk), 
		.in_signal(in_signal), 
		.mute(mute), 
		.bpm(bpm), 
		.led(led), 
		.beep(beep)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		mute = 0;
		forever
		#0.05 clk =~clk;
		// Add stimulus here
	end
	initial begin
		in_signal = 0;
		forever
		#125	in_signal = ~in_signal;
	end
      
endmodule

