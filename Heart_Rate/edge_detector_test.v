`timescale 1ms / 1ps

module edge_detector_test;

	// Inputs
	reg clk;
	reg in_signal;

	// Outputs
	wire detected;

	// Instantiate the Unit Under Test (UUT)
	edge_detector uut (
		.detected(detected), 
		.clk(clk), 
		.in_signal(in_signal)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
      forever
			#0.05	clk = ~clk;
	end
	initial begin
		in_signal = 0;
		forever
			#1	in_signal = ~in_signal;
   end
endmodule

