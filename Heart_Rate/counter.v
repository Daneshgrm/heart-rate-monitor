module counter(output [14:0] cnt,
			input clk,
			input set_zero
    );
	integer cnt_tmp; 
	assign cnt = cnt_tmp;
	initial cnt_tmp = 0 ;
	
	always@(posedge clk)
		if(!set_zero)
			cnt_tmp = cnt_tmp + 1;
		else
			cnt_tmp =  0;
endmodule
