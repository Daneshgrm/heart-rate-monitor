module edge_detector( output reg detected,
				input clk,
				input in_signal
    );
	reg [1:0] signal_level; 
	always@(posedge clk)
		begin
			signal_level = {signal_level[0], in_signal};
			if(signal_level[0] & ~signal_level[1])
				detected = 1;
			else
				detected = 0;
		end
endmodule
