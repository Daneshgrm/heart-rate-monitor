`timescale 1ms / 1ps
module divider_test;

	// Inputs
	reg clk;
	reg [19:0] dividend;
	reg [19:0] divider;

	// Outputs
	wire [19:0] quotient;
	wire [19:0]	remainder;
	wire ready;

	// Instantiate the Unit Under Test (UUT)
	divider uut (
		.quotient(quotient),
		.remainder(remainder),
		.ready(ready), 
		.clk(clk),  
		.dividend(dividend), 
		.divider(divider)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		dividend = 6001;
		divider = 12;
		
		forever
		#0.05	clk = ~clk;
		// Add stimulus here

	end
      
endmodule

