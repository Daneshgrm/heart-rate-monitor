module led_signal_gen(output reg signal,
		input clk,
		input pulse
    );
	
	reg [11:0] counter;
	
	always@(posedge clk)
		begin
			if(signal)	begin
				if(counter[10])
					signal <= 0;
				else
					counter <= counter + 1'b1;
			end
			else begin
				if(pulse) begin
					signal <= 1'b1;
					counter <= 0;
					end
			end
		end
endmodule
