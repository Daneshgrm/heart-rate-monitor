module seven_seg(output [6:0] segments,
			output [2:0] state,
			input clk,
			input [11:0] number
    );
	reg [19:0] cnt = 0;
	reg bcd_number;
	reg [6:0] segments_tmp;
	reg [2:0] state_tmp; 
	assign segments = segments_tmp;
	assign state = state_tmp;
	always@(posedge clk)
		begin
			cnt = cnt + 1;
			if(cnt%3 == 0) begin
				state_tmp <= 3'b110;
				bcd_number <= number[3:0];
			end
			else if(cnt%3 == 1) begin
				state_tmp <= 3'b101;
				bcd_number <= number[7:4];
			end
			if(cnt%3 == 0) begin
				state_tmp <= 3'b011;
				bcd_number <= number[11:8];
			end
		end
	always @(*)
		begin
			case(bcd_number)
				0 : segments_tmp = 7'b1111110;
				1 : segments_tmp = 7'b0110000;
				2 : segments_tmp = 7'b1101101;
				3 : segments_tmp = 7'b1111001;
				4 : segments_tmp = 7'b0110011;
				5 : segments_tmp = 7'b1011011;
				6 : segments_tmp = 7'b1011111;
				7 : segments_tmp = 7'b1110000;
				8 : segments_tmp = 7'b1111111;
				9 : segments_tmp = 7'b1111011;
		default : segments_tmp = 7'b0000000;
			endcase
		end
endmodule
