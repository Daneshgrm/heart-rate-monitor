`timescale 1ns / 1ps
module binary_bcd_test;

	// Inputs
	reg [19:0] number;

	// Outputs
	wire [11:0] bcd_number;

	// Instantiate the Unit Under Test (UUT)
	binary_bcd uut (
		.bcd_number(bcd_number), 
		.number(number)
	);

	initial begin
		// Initialize Inputs
		number = 255;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

