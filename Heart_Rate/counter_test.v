`timescale 1ms / 1ps
module counter_test;

	// Inputs
	reg clk;
	reg set_zero;

	// Outputs
	wire [14:0] cnt;

	// Instantiate the Unit Under Test (UUT)
	counter uut (
		.cnt(cnt), 
		.clk(clk), 
		.set_zero(set_zero)
	);

	initial begin
		clk = 0;
		set_zero = 0;
		forever
		#0.05	clk = ~clk; 
	end
      
endmodule

