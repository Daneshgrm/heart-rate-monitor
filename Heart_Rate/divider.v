module divider(quotient, remainder, ready, clk, dividend, divider);

   input [19:0]  dividend,divider;
   input         clk;
   output [19:0] quotient, remainder;
   output        ready;
   reg [19:0]    quotient_tmp;
   reg [39:0]    dividend_tmp, divider_tmp, diff;
	wire [19:0] remainder = dividend_tmp[19:0];

   reg [4:0]     bit_numbers;
   wire          ready = !bit_numbers;

   initial bit_numbers = 0;
	assign quotient = quotient_tmp;
   always @( posedge clk )

     if ( ready) begin

        bit_numbers = 20;
        quotient_tmp = 0;
        dividend_tmp = {20'd0,dividend};
        divider_tmp = {1'b0,divider,19'd0};

     end else begin

        diff = dividend_tmp - divider_tmp;
        quotient_tmp = { quotient_tmp[18:0], ~diff[39] };
        divider_tmp = { 1'b0, divider_tmp[39:1] };
        if ( !diff[39] ) dividend_tmp = diff;
        bit_numbers = bit_numbers - 1;

     end

endmodule
