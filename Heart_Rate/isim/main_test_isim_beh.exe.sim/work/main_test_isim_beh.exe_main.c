/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000002153895378_4129626524_init();
    work_m_00000000003811019979_2582214024_init();
    work_m_00000000003889718894_2709036157_init();
    work_m_00000000001950900152_1279674626_init();
    work_m_00000000000862289715_3684608150_init();
    work_m_00000000003345821515_1919348379_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000003345821515_1919348379");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
