`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:56:10 01/25/2019
// Design Name:   edge_detector
// Module Name:   E:/Danesh/SUT/Logic Circuit/Lab/Project/Heart_Rate/Test.v
// Project Name:  Heart_Rate
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: edge_detector
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Test;

	// Inputs
	reg clk;
	reg in_signal;

	// Outputs
	wire detected;

	// Instantiate the Unit Under Test (UUT)
	edge_detector uut (
		.detected(detected), 
		.clk(clk), 
		.in_signal(in_signal)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		in_signal = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

