`timescale 1ms / 1ps
module freq_divider_test;

	// Inputs
	reg clk;

	// Outputs
	wire signal;

	// Instantiate the Unit Under Test (UUT)
	freq_divider uut (
		.signal(signal), 
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		clk = 0;

		// Wait 100 ns for global reset to finish
		forever
		#0.05 clk = ~clk;
        
		// Add stimulus here

	end
      
endmodule

