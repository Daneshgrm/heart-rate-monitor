module main_module(
		input clk,
		input in_signal,
		input mute,
		output wire [19:0] bpm,
		output reg led,
		output reg beep
    );
	
	reg [19:0] time_interval = 600000;
	reg [19:0] time_tmp;
	reg set_zero;
	reg mute_flag = 0;
	
	wire [14:0] count;
	wire edge_detected ;
	wire led_pulse;
	wire ready;
	wire junk_remainder;
	wire [11:0] bcd_bpm;
	wire [6:0] segments;
	wire [2:0] segment_select;
	wire [19:0] bpm_tmp;
	reg [19:0]	bpm_copy = 0;
	
	edge_detector detector(edge_detected, clk, in_signal);
	counter cnt(count, clk, set_zero);
	led_signal_gen led_gen(led_pulse, clk, edge_detected);
	divider div(bpm_tmp, junk_remainder, ready, clk, time_interval, time_tmp);
	//binary_bcd bpm2bcd(bcd_bpm, clk, bpm);
	//seven_seg(segments, segment_select, clk, bcd_bpm);
	
	assign bpm = bpm_copy;
	always@(posedge clk) 
		begin
			led <= led_pulse;
			if(mute)
				mute_flag = ~mute_flag;
			if(mute_flag)
				beep <= 0;
			else
				beep <= led_pulse;
			if(edge_detected)	begin
				set_zero <= 1;
				time_tmp <= {5'b00000, count};
			end
			else
				set_zero <= 0;
			if(ready)
				bpm_copy <= bpm_tmp;
		end
endmodule
